import de.htwberlin.fiw.profiler.ProfiledClass;
import de.htwberlin.fiw.profiler.Profiler;


public class Main extends ProfiledClass {

    public void run() {
    }

    public static void main(String[] args) {

        Runnable myRunnable = new MyRunnable(args);
        new Thread(myRunnable).start();

        //Profiler creation and check speed
        Profiler profiler = new Profiler(Main.class);
        profiler.start();
        profiler.printResults();

    }

}










