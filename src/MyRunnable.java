import java.io.File;

public class MyRunnable implements Runnable {

    private String[] args;

    public MyRunnable(String[] args) {
        this.args = args;
    }

    public void run(){

        File file = new File(args[0]);
        Utils utils = new Utils(Utils.getLengthOfData(file));
        int[] data = utils.getData(file);
        int cores = utils.getCores();
        int[][] result = utils.devideIntoSubArrays(cores, data);
        MyThread workers[] = new MyThread[cores];
        /**
         * Creation of threads objects and filling up list
         */
        for (int i = 0; i < cores; i++) {
            MyThread thread = new MyThread(result[i]);
            workers[i] = thread;
        }

        int[] endResult = new int[data.length];
        /**
         * Start of threads presorting
         */
        for (int i = 0; i < workers.length; i++) {
            workers[i].start();
        }
        /**
         * Threads join
         */
        for (int j = 0; j < workers.length; j++) {
            try {
                workers[j].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        /**
         * Get all data from threads and merge it into one array
         */
        for (int a = 0; a < workers.length; a++) {
            for (int b = 0; b < workers[a].getData().length; b++) {
                endResult[b + (a * workers[a].getData().length)] = workers[a].getData()[b];
            }
        }
        if (cores > 1) {
            Sort bitonicSort = new Sort();
            int finalArray[] = endResult;
            int up = 1;
            /**
             * Start end sorting
             */
            bitonicSort.sort(finalArray, finalArray.length, up);
        }
        for (int q = 0; q < endResult.length; q++) {
            if (q == Integer.parseInt(args[1]))
                System.out.println("Element am Index " + q + ":" + endResult[q]);
        }
    }
}
