/**
 * This class inherits java.lang.Thread class and overrides run method to start parallel execution of multithreading
 *
 */

public class MyThread extends Thread {
    private int[] data;
    private Sort bitonicSort = new Sort();


    public MyThread(int[] data) {
        this.data = data;
    }

    public int[] getData() {
        return this.data;
    }

    public void setData(int[] data) {
        this.data = data;
    }

    public void run() {
        this.bitonicSort.sort(this.data,this.data.length,1);
    }
}
