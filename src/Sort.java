/**
 * This class is in fact the implementation of bitonic merge sort.
 * <p>
 * Please see the official Repo under  {@link https://github.com/Nadja7/bitonic-sort/tree/master}  for detailed description of implementation.
 */

public class Sort {
    /**
     * This method calls bitonicSort method with following parameters:
     *
     * @param int a[] represents initial data
     * @param int dataLength acceptable length of data
     * @param int dir selected direction for final sorting --> 1:ascending, 0:decrescent
     */
    public void sort(int a[], int dataLength, int dir) {
        bitonicSort(a, 0, dataLength, dir);
    }

    /**
     * In this method, initial data  which is coming from sort method is divided into sub sequences (sub arrays) using recursion and  given direction.
     * finally the merge function will be called, which accordingly joins sorted sub arrays
     *
     * @param int a[] represents initial data
     * @param int from index position for sequence to be sorted
     * @param int dataLength acceptable length of data
     * @param int dir selected direction for final sorting --> 1:ascending, 0:decrescent
     */
    public void bitonicSort(int a[], int from, int dataLength, int dir) {
        if (dataLength > 1) {
            int sequenceLength = dataLength / 2;
            bitonicSort(a, from, sequenceLength, 1);
            bitonicSort(a, from + sequenceLength, sequenceLength, 0);
            merge(a, from, dataLength, dir);
        }
    }

    /**
     * Merge function joins sub sequences ascending accordingly power of two
     *
     * @param int a[]--> represents initial data
     * @param int from -->index position for sequence to be sorted
     * @param int numbersOfElements -->number elements to be sorted
     * @param int dir --> selected direction for final sorting --> 1:ascending, 0:decrescent
     */
    public void merge(int a[], int from, int numbersOfElements, int dir) {
        if (numbersOfElements > 1) {
            int sequenceLength = numbersOfElements / 2;
            for (int i = from; i < from + sequenceLength; i++)
                compareAndSwap(a, i, i + sequenceLength, dir);
            merge(a, from, sequenceLength, dir);
            merge(a, from + sequenceLength, sequenceLength, dir);
        }
    }

    /**
     * This function compares and changes elements accordingly
     *
     * @param int a[] represents initial data
     * @param int i first element for comparing and swapping
     * @param int j second element
     * @param int dir selected direction for final sorting --> 1:ascending, 0:decrescent
     */
    public void compareAndSwap(int a[], int i, int j, int dir) {
        if ((a[i] > a[j] && dir == 1) || (a[i] < a[j] && dir == 0)) {
            int temp = a[i];
            a[i] = a[j];
            a[j] = temp;
        }
    }

}
