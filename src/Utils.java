import java.io.*;

/**
 * This class contains all the necessary utility functions for the bitonic algorithm which is implemented in Sort.java class.
 * <p>
 * Please see the official Repo under  {@link https://github.com/Nadja7/bitonic-sort/tree/master}  for detailed description of implementation.
 */
public class Utils {
    private int[] data;

    public Utils(int length) {
        this.data = new int[length];
    }

    /**
     * This method reads data from given file and fill array with data
     *
     * @param File file--> initial file for streaming out
     * @return int[] data--> filled array
     */
    public int[] getData(File file) {
        try {
            DataInputStream input = new DataInputStream(new FileInputStream(
                    file));

            for (int i = 0; i < this.data.length; i++) {
                this.data[i] = input.readInt();
            }
            input.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("");
            System.out.println("Reached end of file");
        }

        return this.data;
    }

    /**
     * This method is used to get information about available cores of system
     *
     * @return int cores--> number of available cores
     */
    public int getCores() {

        int cores = Runtime.getRuntime().availableProcessors();
        return cores;

    }

    /**
     * This method divides the original data into subarrays according to the number of available cores
     *
     * @param int   cores --> number of available cores
     * @param int[] data --> initial Data  This is the second parameter to addNum method
     * @return int[][] result --> two dimensional array where length of first dimension is equal to number of cores(==threads) and second contains subarray
     */
    public int[][] devideIntoSubArrays(int cores, int[] data) {

        int unit = data.length / cores;
        int[] piece = new int[unit];
        int[][] result = new int[cores][unit];
        for (int i = 0; i < cores; i++) {
            for (int j = 0; j <= unit - 1; j++) {
                piece[j] = data[j + (unit * i)];
                result[i][j] = data[j + (unit * i)];
            }
        }
        return result;
    }

    /**
     * This method defines length of file which is used to set length of array as class attribute
     *
     * @param File file--> initial file
     * @return int arrayLength --> length for class attribute array
     */
    public static int getLengthOfData(File file) {
        long dataSize = file.length();
        int convertedSize = (int) dataSize;
        int arrayLength = convertedSize / 4;
        return arrayLength;
    }
}

