[[_TOC_]]

# Bitonic mergesort
This is a [Java SE 8](https://docs.oracle.com/javase/8/docs/api/) implementation of the [bitonic sort algorithm](https://en.wikipedia.org/wiki/Bitonic_sorter), originally devised by [Ken Batcher](https://en.wikipedia.org/wiki/Ken_Batcher). The bitonic mergesort is a parallel comparison-based sorting algorithm which can also be used as a construction method for building a sorting network. 

The bitonic sort only works on sequences with a length which is in the order of the power of 2, and it is based 
on the concept of converting a given input sequence into a bitonic sequence. A bitonic sequence is a sequence of numbers which is first strictly increasing then after a point strictly decreasing. A single increasing or decreasing sequence is also bitonic, as the complementary decreasing or increasing sequence is considered to be empty. 

# Example: A bitonic sorting network with 16 inputs

![bitonic sort](images/BitonicSort1.png)
*Source: [Wikipedia](https://en.wikipedia.org/wiki/Bitonic_sorter#/media/File:BitonicSort1.svg)*

The numbers to be sorted move from left to right on the horizontal lines. On their way, the numbers are compared several times. The arrows represent the comparators which compare two numbers at varying distances and ensure that the arrow points toward the larger number. If necessary, the numbers are swapped to put them in the correct order.


# Multithreading
As a [parallel algorithm](https://en.wikipedia.org/wiki/Parallel_algorithm), the bitonic mergesort is an algorithm which performs multiple actions simultaneously. In doing so, it can contribute to improving the sorting process's performance by making it faster. To make full use of the performance advantages of a parallel sorting algorithm, this implementation of the bitonic mergesort uses [multithreaded execution](https://docs.oracle.com/javase/tutorial/essential/concurrency/procthread.html).


# Framework
Built with [Java](https://www.java.com/)

# Installation

1. Get the source code by cloning the repository, e.g. via https: `git clone https://github.com/Nadja7/bitonic-sort.git`

2. Compile the source code by running the following command inside the project's `src`-directory: `javac -cp <PATH_TO__EXTERNAL_PROFILER.JAR> Main.java`


# How to use?

Please note that the bitonic mergesort only works on sequences with a length from the order of the power of 2, i.e. 2, 4, 8, 16, and so on. Make sure your input data meets this requirement.
The current implementation requires `int`-numbers as input values. 
Make sure that project is already compiled, otherwise compile the `Main` class with `javac` first from `src`-directory (see #Installation).

The bitonic sort can be run via the command line with the following command executed from inside the project's `src`-directory:
````
java -cp <PATH_TO_EXTERNAL_PROFILER.JAR>:. Main <PATH-TO-INPUT-FILE> <INDEX>
````
The index parameter indicates the index of the number which will be printed after the sorting process has finished. 

# Implementation

## Utils

The class `Utils` contains all necessary utility functions for the bitonic sort algorithm. 

The class contains the `int[]`-array `data` which serves to store and process the initial input data. 

Its methods serve to get the input data and create an array on which the sorting algorithm is executed as well as to get the number of cores which are available for threading on the processor which is used, and to divide the initial array created from the input data into several subarrays which are then processed parallely.

## MyThread

The class `MyThread` serves to create the threads necessary for the multithreaded execution of the bitonic sort algorithm and implements the `run`-method which calls the `sort`-method implemented in the class `Sort`.


## Sort
The class `Sort` is the actual implementation of the bitonic mergesort algorithm. 
In the `sort`-method, initial data is divided into sub-sequences (sub-arrays) using recursion and a given direction (1 for ascending, 0 for descending order).
Finally, the `merge`-function will be called, which joins the sub-arrays again after they have been sorted. 
The comparison action takes place via the `compareAndSwap`-method which is called inside the `merge`-method. This is the method which iterates over the respective sub-array, compares the numbers and sorts them according to the given direction, just like the arrow-comparators in the example image above.


## Main
This class implements the `main`-method in which the input data is read from a file, then divided into subarrays according to the number of cores available for the multithreading.

The bitonic sort is then executed simulatenously on the multiple sub-arrays in multiple threads. 

In the end, the threads are joined and the `sort`-method is executed once again in order to sort all the input data together in one array and to get the complete, sorted array as a result. 


# Performance tests
Our implementation of the bitonic sort was tested several times in terms of performance against an example implementation. In the comparative tests, results would show that our implementation was *XXX* faster than the example implementation, and that it used *XXX* of memory compared to the example implementation. 


# Credits
Built by Nadja Kondratova, Elisabeth Steffen, Anna Sokolov, Alla Massold & Natalie Basedow


# License
- [GNU GPL v3.0](https://choosealicense.com/licenses/gpl-3.0/)
